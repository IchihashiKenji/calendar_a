<?php //PSRと関数わけ以外
$y = isset($_GET['y']) ? $_GET['y'] : date("Y");
//↑もし、URLにyがセットされていたらｙを使い、そうでなければ現在の年を使う。

$timeStamp = strtotime($y . "-01");

if ($timeStamp === false) {
    $timeStamp = time();

}
//↑もしURLに不正な値が入った場合、現在のカレンダーを表示

function getweeks($currentMonth){
    global $y;
    global $timeStamp;

    $lastDay = 0;
    $youbi = 0;

    $lastDay = date("t", strtotime($y . "-" . $currentMonth . "-1"));
    //↑月の最終日の取得
    $youbi = date("w", strtotime($y . "-" . $currentMonth . "-1"));
    //↑月の１日の曜日番号取得（日=0から土=6）

    $weeks = array();
    $week = '';

    $youbi = ($youbi == 0) ? 7 : $youbi;
    $week .= str_repeat('<td></td>', $youbi - 1);
        //↑月初めの隙間の数。日曜始まり以外は（曜日番号-1)個。
    $holidayData = file("pub.txt",FILE_IGNORE_NEW_LINES);
    //↑ファイルの呼び出しと配列化、文末文字の無効化

    for ($day = 1; $day <= $lastDay; $day++, $youbi++) {
    //↑その月が終わるまで、日付と曜日を１づつ増やしていく。

        $now = $y . '/' . $currentMonth . '/' . "$day";
        $publicHoliday = array_search($now, $holidayData);
        //外部ファイル（祝日データ）に当日が該当するか判断

        $class = '';
        if ($publicHoliday == TRUE || $publicHoliday === 0) {
            $class = "redday";
        } else {
            $class = '';
        }
        //↑祝日データにヒットした場合、reddayクラスを付与。

        $week .= sprintf('<td class="youbi_%d %s">%d</td>', $youbi % 7 , $class , $day);
        //↑日付の出力
        if ($youbi % 7 == 0 OR $day == $lastDay) {
            if ($day == $lastDay && $youbi % 7 != 0) {
                    $week .= str_repeat('<td></td>', 7 - ($youbi % 7));
                    //↑終わりの隙間の数。日曜終わり以外は{7-曜日番号%7}個
            }
            $weeks[] = '<tr>' . $week . '</tr>';
            //↑週ごとに行を変える。
            $week = '';
            //↑次の月に備え、日付を空っぽにする。
        }
    }
    return $weeks;
    //↑行ごと整頓された空白と日付を出力。
}

$prev = $y-1;
$next = $y+1;
//↑前年ページと翌年ページを作る

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>PHPカレンダー</title>
<link rel="stylesheet" href="P47calendar0507.css">
</head>
<body>
    <header>
        <p class="years">
            <a href="?y=<?php echo $prev; ?>">&laquo;</a>
            <?php echo $y; ?>
            <a href="?y=<?php echo $next; ?>">&raquo;</a>
            <!-- ↑前年、翌年へのリンクの設置 -->
        </p>
    </header>
    <ul>
        <?php
        $month = array("1","2","3","4","5","6","7","8","9","10","11","12");
        foreach ($month as $thisMonth) {
        ?>
            <li>
            <table class="mon<?php echo $thisMonth; ?>">
                <thead>
                    <tr>
                        <th colspan="7">
                            <?php
                            echo $thisMonth;
                            ?>
                        </th>
                    </tr>
                    <tr>
                        <th>月</th>
                        <th>火</th>
                        <th>水</th>
                        <th>木</th>
                        <th>金</th>
                        <th>土</th>
                        <th>日</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $weeks = getweeks($thisMonth);
                    foreach ($weeks as $week) {
                        echo $week;
                    }
                    ?>
                    <!-- ↑日付の呼び出し -->
                </tbody>
            </table>
            </li>
        <?php
        }
        ?>
    </ul>
</body>
</html>